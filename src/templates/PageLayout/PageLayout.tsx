import React, { PureComponent, ReactNode } from 'react'
import { ThemeProvider } from 'styled-components'

import GlobalStyle from '@styles/GlobalStyle'
import { sbTheme } from '@themes/sbTheme'

import { PageLayoutProps } from './PageLayout.types'

export default class PageLayout extends PureComponent<PageLayoutProps> {
  render(): ReactNode {
    return (
      <ThemeProvider theme={sbTheme}>
        <GlobalStyle />
        {this.props.children}
      </ThemeProvider>
    )
  }
}
