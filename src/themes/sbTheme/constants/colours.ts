import { Colours } from './colours.types'

const colours: Colours = {
  black: '#131415',
  grey: '#727170',
  gold: '#c29f7c',
  white: '#ffffff',
}

export default colours
