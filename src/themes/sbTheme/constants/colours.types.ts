export type colorsTypeKeys = 'black' | 'grey' | 'gold' | 'white'

export type Colours = {
  [key in colorsTypeKeys]: string
}
